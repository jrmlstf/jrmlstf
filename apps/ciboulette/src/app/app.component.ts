import { Component } from '@angular/core';

@Component({
  selector: 'ciboulette-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {
  title = 'ciboulette';
}
