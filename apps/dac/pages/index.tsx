import React from 'react';

import styles from './index.module.scss';

export const Index = () => {
    return (
        <div className={styles.page}>
            <img src="/star.svg" className="material-icons" alt="" />
        </div>
    );
};

export default Index;
