import { enableProdMode } from '@angular/core';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
import { environment } from '@jrmlstf/dicomo/environment';
import { RootModule } from '@jrmlstf/dicomo/root';

if (environment.production) {
    enableProdMode();
}

platformBrowserDynamic()
    .bootstrapModule(RootModule)
    .catch((err) => console.error(err));
