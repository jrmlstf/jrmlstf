import { isPlatformServer } from '@angular/common';
import {
    AfterContentInit,
    Directive,
    ElementRef,
    Inject,
    Input,
    OnChanges,
    PLATFORM_ID,
} from '@angular/core';

@Directive({
    selector: '[cdkAutoFocus]',
})
export class AutoFocusDirective implements OnChanges, AfterContentInit {
    @Input() public set dicomoQuestionAutoFocus(isActive: boolean | string) {
        if (typeof isActive === 'string') {
            this.isActive = true;
            return;
        }

        this.isActive = isActive;
    }

    private initialized: boolean = false;
    private isActive: boolean = true;

    constructor(
        private readonly elementRef: ElementRef,
        @Inject(PLATFORM_ID) private readonly platformId: Object
    ) {}

    public ngOnChanges(): void {
        if (this.initialized) {
            this.setupFocus();
        }
    }

    public ngAfterContentInit(): void {
        this.initialized = true;
        this.setupFocus();
    }

    private setupFocus(): void {
        if (isPlatformServer(this.platformId) || !this.isActive) {
            return;
        }

        setTimeout(() => this.focus());
    }

    private focus(): void {
        this.elementRef.nativeElement.focus();
    }
}
