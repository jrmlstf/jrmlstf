import { Directive, OnDestroy } from '@angular/core';
import { Subject } from 'rxjs';

@Directive()
export abstract class AbstractOnDestroyNotifier implements OnDestroy {
    protected readonly destroyed$: Subject<void> = new Subject<void>();

    public ngOnDestroy(): void {
        this.destroyed$.next();
        this.destroyed$.complete();
    }
}
