import { Component, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { AbstractOnDestroyNotifier } from './abstract-on-destroy-notifier.class';

@Component({})
class AbstractOnDestroyNotifierTestHost extends AbstractOnDestroyNotifier {}

describe('AbstractOnDestroyNotifier', () => {
    let fixture: ComponentFixture<AbstractOnDestroyNotifierTestHost>;
    let component: AbstractOnDestroyNotifierTestHost;

    beforeEach(() => {
        TestBed.configureTestingModule({
            declarations: [AbstractOnDestroyNotifierTestHost],
            schemas: [CUSTOM_ELEMENTS_SCHEMA],
        });

        fixture = TestBed.createComponent(AbstractOnDestroyNotifierTestHost);
        component = fixture.componentInstance;
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
