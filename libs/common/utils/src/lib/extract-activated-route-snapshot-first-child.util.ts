import { ActivatedRouteSnapshot } from '@angular/router';

export const extractActivatedRouteSnapshotFirstChild = (
    snapshot: ActivatedRouteSnapshot
): ActivatedRouteSnapshot => {
    if (snapshot.firstChild) {
        return extractActivatedRouteSnapshotFirstChild(snapshot.firstChild);
    }

    return snapshot;
};
