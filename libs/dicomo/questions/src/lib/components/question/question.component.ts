import {
    ChangeDetectionStrategy,
    Component,
    ElementRef,
    EventEmitter,
    Input,
    OnChanges,
    OnInit,
    Output,
    QueryList,
    SimpleChanges,
    ViewChildren,
} from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AbstractOnDestroyNotifier, mapEnum } from '@jrmlstf/common/utils';
import { PlayLevelEnum, PlayModeEnum, Word } from '@jrmlstf/dicomo/types';
import isNil from 'lodash-es/isNil';

@Component({
    selector: 'dicomo-questions-question',
    templateUrl: './question.component.html',
    styleUrls: ['./question.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class QuestionComponent extends AbstractOnDestroyNotifier
    implements OnChanges, OnInit {
    @ViewChildren('autofocus', { read: ElementRef })
    public readonly autofocusItems: QueryList<ElementRef>;

    @Input() public readonly word: Word;
    @Input() public readonly answers: Word[];
    @Input() public readonly level: PlayLevelEnum;

    @Output() public readonly found: EventEmitter<void> = new EventEmitter();

    public answerForm: FormGroup;
    public result: boolean;
    public isInputMode: boolean;
    public isSelectMode: boolean;
    public mode: PlayModeEnum;

    constructor(private readonly formBuilder: FormBuilder) {
        super();
    }

    public ngOnChanges(changes: SimpleChanges): void {
        if (changes?.level) {
            this.setModeFromLevel(changes.level.currentValue);
        }
    }

    public ngOnInit(): void {
        this.answerForm = this.initAnswerForm();
    }

    public validateAnswer(goodAnswer: Word): void {
        this.result = this.answerForm.get('answer').value === goodAnswer.word;
        this.answerForm.get('answer').setValue('');
    }

    public hasResult(): boolean {
        return !isNil(this.result);
    }

    public next(): void {
        this.found.emit();
        this.resetResult();
    }

    public retry(): void {
        this.resetResult();
    }

    private setModeFromLevel(level: PlayLevelEnum): void {
        this.mode = mapEnum(PlayLevelEnum, PlayModeEnum, level);
        this.isInputMode = this.mode === PlayModeEnum.INPUT;
        this.isSelectMode = this.mode === PlayModeEnum.SELECT;
    }

    private resetResult(): void {
        this.result = null;
    }

    private initAnswerForm(): FormGroup {
        return this.formBuilder.group({
            answer: ['', [Validators.required]],
        });
    }
}
