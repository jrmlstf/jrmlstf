import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { AutoFocusDirectiveModule } from '@jrmlstf/common/cdk';
import { QuestionComponent } from './question.component';

@NgModule({
    declarations: [QuestionComponent],
    imports: [CommonModule, ReactiveFormsModule, AutoFocusDirectiveModule],
    exports: [QuestionComponent],
})
export class QuestionComponentModule {}
