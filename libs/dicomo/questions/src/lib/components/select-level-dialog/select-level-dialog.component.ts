import { ChangeDetectionStrategy, Component } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';
import { PlayLevelEnum } from '@jrmlstf/dicomo/types';

@Component({
    selector: 'dicomo-questions-select-level-dialog',
    templateUrl: './select-level-dialog.component.html',
    styleUrls: ['./select-level-dialog.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SelectLevelDialogComponent {
    public playLevelEnum: typeof PlayLevelEnum = PlayLevelEnum;

    constructor(
        public dialogRef: MatDialogRef<
            SelectLevelDialogComponent,
            PlayLevelEnum
        >
    ) {}

    public selectLevel(level: PlayLevelEnum): void {
        this.dialogRef.close(level);
    }
}
