import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { MatDialogModule } from '@angular/material/dialog';
import { SelectLevelDialogComponent } from './select-level-dialog.component';

@NgModule({
    declarations: [SelectLevelDialogComponent],
    imports: [CommonModule, MatDialogModule, ReactiveFormsModule],
    exports: [SelectLevelDialogComponent],
})
export class SelectLevelDialogComponentModule {}
