import { NgModule } from '@angular/core';
import { CoreModule } from '@jrmlstf/dicomo/core';
import { RootComponent } from './components/root/root.component';
import { RootComponentModule } from './components/root/root.component.module';

@NgModule({
    imports: [CoreModule, RootComponentModule],
    bootstrap: [RootComponent],
})
export class RootModule {}
