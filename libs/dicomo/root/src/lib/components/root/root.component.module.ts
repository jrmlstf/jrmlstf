import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { LayoutComponentModule } from '@jrmlstf/dicomo/layout';
import { RootComponent } from './root.component';

@NgModule({
    declarations: [RootComponent],
    imports: [BrowserAnimationsModule, CommonModule, LayoutComponentModule],
    exports: [RootComponent],
})
export class RootComponentModule {}
