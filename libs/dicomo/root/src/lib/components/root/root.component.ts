import { animate, style, transition, trigger } from '@angular/animations';
import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { delay, startWith, take } from 'rxjs/operators';

@Component({
    selector: 'dicomo-root',
    templateUrl: './root.component.html',
    styleUrls: ['./root.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
    animations: [
        trigger('leaveAnimation', [
            transition(':leave', [animate('100ms', style({ opacity: 0 }))]),
        ]),
    ],
})
export class RootComponent implements OnInit {
    public readonly loadingSubject: BehaviorSubject<
        boolean
    > = new BehaviorSubject(false);
    public loading$: Observable<boolean>;

    public ngOnInit(): void {
        this.loading$ = this.loadingSubject
            .asObservable()
            .pipe(delay(100), startWith(true), take(2));
    }
}
