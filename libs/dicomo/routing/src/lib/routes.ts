import { Route } from '@angular/router';

export const routes: Route[] = [
    {
        path: '',
        loadChildren: () =>
            import('@jrmlstf/dicomo/homepage').then(
                (m: any) => m.HomePageModule
            ),
    },
    {
        path: 'play',
        loadChildren: () =>
            import('@jrmlstf/dicomo/play').then((m: any) => m.PlayPageModule),
    },
    {
        path: '**',
        redirectTo: '',
    },
];
