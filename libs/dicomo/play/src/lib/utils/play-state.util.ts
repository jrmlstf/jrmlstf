import { PlayStateEnum } from '../enums/play-state.enum';

export const isChooseLevel: IsPlayStateFn = (state: PlayStateEnum) => {
    return state === PlayStateEnum.CHOOSE_LEVEL;
};

export const isPlaying: IsPlayStateFn = (state: PlayStateEnum) => {
    return state === PlayStateEnum.PLAYING;
};

export const isFinished: IsPlayStateFn = (state: PlayStateEnum) => {
    return state === PlayStateEnum.FINISHED;
};

export type IsPlayStateFn = (state: PlayStateEnum) => boolean;
