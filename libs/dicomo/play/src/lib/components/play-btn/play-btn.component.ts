import { Component } from '@angular/core';

@Component({
    selector: 'dicomo-play-btn',
    templateUrl: './play-btn.component.html',
    styleUrls: ['./play-btn.component.scss'],
})
export class PlayBtnComponent {}
