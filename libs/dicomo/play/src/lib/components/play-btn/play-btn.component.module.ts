import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { MatButtonModule } from '@angular/material/button';
import { RouterModule } from '@angular/router';
import { PlayBtnComponent } from './play-btn.component';

@NgModule({
    declarations: [PlayBtnComponent],
    imports: [CommonModule, RouterModule, MatButtonModule],
    exports: [PlayBtnComponent],
})
export class PlayBtnComponentModule {}
