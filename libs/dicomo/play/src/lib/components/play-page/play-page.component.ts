import {
    ChangeDetectionStrategy,
    ChangeDetectorRef,
    Component,
    OnInit,
} from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { AbstractOnDestroyNotifier, getRandomInt } from '@jrmlstf/common/utils';
import { SelectLevelDialogComponent } from '@jrmlstf/dicomo/questions';
import { PlayLevelEnum, Word } from '@jrmlstf/dicomo/types';
import { WordsFacade } from '@jrmlstf/dicomo/words';
import { combineLatest, Observable } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { PlayStateEnum } from '../../enums/play-state.enum';
import {
    isChooseLevel,
    isFinished,
    isPlaying,
    IsPlayStateFn,
} from '../../utils/play-state.util';

@Component({
    selector: 'dicomo-play-page',
    templateUrl: './play-page.component.html',
    styleUrls: ['./play-page.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PlayPageComponent extends AbstractOnDestroyNotifier
    implements OnInit {
    public wordQuestion: Word = null;
    public answers: Word[] = [];
    public level: PlayLevelEnum = PlayLevelEnum.BEGINNER;
    public state: PlayStateEnum = PlayStateEnum.CHOOSE_LEVEL;
    public isChooseLevel: IsPlayStateFn = isChooseLevel;
    public isPlaying: IsPlayStateFn = isPlaying;
    public isFinished: IsPlayStateFn = isFinished;
    public playStateEnum: typeof PlayStateEnum = PlayStateEnum;

    private wordsToGuess: Word[] = [];

    constructor(
        private readonly wordsFacade: WordsFacade,
        private readonly dialog: MatDialog,
        private readonly cdr: ChangeDetectorRef
    ) {
        super();
    }

    public ngOnInit(): void {
        combineLatest([
            this.openSelectLevelDialog(),
            this.wordsFacade.allWords$,
        ])
            .pipe(takeUntil(this.destroyed$))
            .subscribe(([level, words]: [PlayLevelEnum, Word[]]) => {
                this.level = level;
                this.answers = [...words];
                this.wordsToGuess = [...words];
                this.nextQuestion();

                this.state = PlayStateEnum.PLAYING;
                this.cdr.detectChanges();
            });
    }

    public nextQuestion(): void {
        if (!this.wordsToGuess.length) {
            this.state = PlayStateEnum.FINISHED;
        }

        const randomWordPosition: number = getRandomInt(
            0,
            this.wordsToGuess.length
        );
        this.wordQuestion = this.wordsToGuess[randomWordPosition];
        this.wordsToGuess = this.wordsToGuess.filter(
            (_: Word, index: number) => index !== randomWordPosition
        );
    }

    private openSelectLevelDialog(): Observable<PlayLevelEnum> {
        const dialogRef = this.dialog.open(SelectLevelDialogComponent, {
            disableClose: true,
        });
        return dialogRef.afterClosed();
    }
}
