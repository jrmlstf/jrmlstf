import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { MatDialogModule } from '@angular/material/dialog';
import { RouterModule } from '@angular/router';
import {
    QuestionComponentModule,
    SelectLevelDialogComponentModule,
} from '@jrmlstf/dicomo/questions';
import { PlayPageComponent } from './play-page.component';
import { PlayPageRoutingModule } from './play-page.routing.module';

@NgModule({
    declarations: [PlayPageComponent],
    imports: [
        CommonModule,
        RouterModule,
        MatDialogModule,
        SelectLevelDialogComponentModule,
        PlayPageRoutingModule,
        QuestionComponentModule,
    ],
    exports: [PlayPageComponent, MatDialogModule], // TODO Check if export of MatDialogModule is really needed
})
export class PlayPageModule {}
