import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { PlayPageComponent } from './play-page.component';

describe('PlayPageComponent', () => {
    let fixture: ComponentFixture<PlayPageComponent>;
    let component: PlayPageComponent;

    beforeEach(() => {
        TestBed.configureTestingModule({
            declarations: [PlayPageComponent],
            schemas: [CUSTOM_ELEMENTS_SCHEMA],
        });

        fixture = TestBed.createComponent(PlayPageComponent);
        component = fixture.componentInstance;
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
