import { NgModule } from '@angular/core';
import { Route, RouterModule } from '@angular/router';
import {
    headerConfigKey,
    HeaderItemsEnum,
    HeaderSlotEnum,
} from '@jrmlstf/dicomo/header';
import { PlayPageComponent } from './play-page.component';

const routes: Route[] = [
    {
        path: '',
        component: PlayPageComponent,
        data: {
            [headerConfigKey]: {
                [HeaderSlotEnum.LEFT]: [
                    {
                        type: HeaderItemsEnum.CLOSE,
                    },
                ],
            },
        },
    },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
})
export class PlayPageRoutingModule {}
