import { Injectable } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { CanActivate } from '@angular/router';
import { SelectLevelDialogComponent } from '@jrmlstf/dicomo/questions';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
    providedIn: 'root',
})
export class SelectLevelGuard implements CanActivate {
    constructor(private readonly dialog: MatDialog) {}

    public canActivate(): Observable<boolean> {
        return this.openSelectLevelDialog();
    }

    private openSelectLevelDialog(): Observable<boolean> {
        const dialogRef = this.dialog.open(SelectLevelDialogComponent);

        return dialogRef.afterClosed().pipe(map(() => true));
    }
}
