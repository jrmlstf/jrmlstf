import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { HeaderComponentModule } from '@jrmlstf/dicomo/header';
import { PlayBtnComponentModule } from '@jrmlstf/dicomo/play';
import { LayoutComponent } from './layout.component';

@NgModule({
    declarations: [LayoutComponent],
    imports: [
        CommonModule,
        RouterModule,
        HeaderComponentModule,
        PlayBtnComponentModule,
    ],
    exports: [LayoutComponent],
})
export class LayoutComponentModule {}
