export interface Definition {
    value: string;
    example?: string;
    synonyms?: string[];
    antonyms?: string[];
}
