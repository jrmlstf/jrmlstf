import { Meaning } from './meaning.interface';

export interface Word {
    id: string | number;
    word: string;
    meanings: Meaning[];
}
