import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { WordsListComponentModule } from '@jrmlstf/dicomo/words';
import { HomePageComponent } from './home-page.component';
import { HomePageRoutingModule } from './home-page.routing.module';

@NgModule({
    declarations: [HomePageComponent],
    imports: [CommonModule, HomePageRoutingModule, WordsListComponentModule],
    exports: [HomePageComponent],
})
export class HomePageModule {}
