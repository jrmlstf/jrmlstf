import { NgModule } from '@angular/core';
import { Route, RouterModule } from '@angular/router';
import {
    headerConfigKey,
    HeaderItemsEnum,
    HeaderSlotEnum,
} from '@jrmlstf/dicomo/header';
import { HomePageComponent } from './home-page.component';

const routes: Route[] = [
    {
        path: '',
        component: HomePageComponent,
        data: {
            [headerConfigKey]: {
                [HeaderSlotEnum.LEFT]: [
                    {
                        type: HeaderItemsEnum.LOGO,
                    },
                ],
                [HeaderSlotEnum.RIGHT]: [
                    {
                        type: HeaderItemsEnum.ADD_WORD,
                    },
                ],
            },
        },
    },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
})
export class HomePageRoutingModule {}
