export * from './lib/components/list/list.component.module';
export * from './lib/services/words.service';
export * from './lib/store/words.facade';
export * from './lib/store/words.store.module';
