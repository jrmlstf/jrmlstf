import { Word } from '@jrmlstf/dicomo/types';

export const wordsDB: Word[] = [
    {
        id: 1,
        word: 'provende',
        meanings: [
            {
                partOfSpeech: 'nom féminin',
                definitions: [
                    {
                        value: 'Provisions de bouche, vivres.',
                    },
                    {
                        value:
                            'Nourriture donnée au bétail et aux animaux de bassecour.',
                    },
                ],
            },
        ],
    },
    {
        id: 2,
        word: 'commensal',
        meanings: [
            {
                partOfSpeech: 'nom et adjectif',
                definitions: [
                    {
                        value:
                            "Personne qui mange habituellement à la même table qu'une ou plusieurs autres.",
                        synonyms: ['hôte'],
                    },
                ],
            },
        ],
    },
    {
        id: 3,
        word: 'turgescence',
        meanings: [
            {
                partOfSpeech: 'nom féminin',
                definitions: [
                    {
                        value:
                            'Augmentation de volume par rétention de sang veineux.',
                        example: 'Turgescence du pénis.',
                        synonyms: ['tumescence'],
                        antonyms: [],
                    },
                    {
                        value: "Dureté due à l'afflux d'eau.",
                    },
                ],
            },
        ],
    },
    {
        id: 4,
        word: 'ergoter',
        meanings: [
            {
                partOfSpeech: 'verbe intransitif',
                definitions: [
                    {
                        value:
                            'Trouver à redire sur des points de détail, des choses insignifiantes.',
                        example: "Vous n'allez pas ergoter pour un euro !",
                        synonyms: ['chicaner', 'discuter', 'pinailler'],
                        antonyms: [],
                    },
                ],
            },
        ],
    },
    {
        id: 5,
        word: 'sérendipité',
        meanings: [
            {
                partOfSpeech: 'nom féminin',
                definitions: [
                    {
                        value:
                            "Capacité, aptitude à faire par hasard une découverte inattendue et à en saisir l'utilité (scientifique, pratique).",
                    },
                ],
            },
        ],
    },
    {
        id: 6,
        word: 'dessiccation',
        meanings: [
            {
                partOfSpeech: 'nom féminin',
                definitions: [
                    {
                        value: "Élimination de l'humidité d'un corps.",
                        example: "Vous n'allez pas ergoter pour un euro !",
                        synonyms: ['déshydratation', 'lyophilisation'],
                        antonyms: [],
                    },
                ],
            },
        ],
    },
];
