import { Injectable } from '@angular/core';
import { Word } from '@jrmlstf/dicomo/types';
import { Observable, timer } from 'rxjs';
import { map } from 'rxjs/operators';
import { wordsDB } from '../data/words.db';

@Injectable({
    providedIn: 'root',
})
export class WordsApi {
    public getWords(): Observable<Word[]> {
        return timer(1000).pipe(map(() => wordsDB));
    }
}
