import { Injectable } from '@angular/core';
import { Word } from '@jrmlstf/dicomo/types';
import { Actions, ofType } from '@ngrx/effects';
import { Observable } from 'rxjs';
import { wordsActions } from '../store/words.actions';
import { WordsFacade } from '../store/words.facade';
import { WordsApi } from './words.api';

@Injectable({
    providedIn: 'root',
})
export class WordsService {
    public loadWordsAndGetNotifiedWhenSuccess(): Observable<void> {
        this.wordsFacade.loadWords();
        return this.actions$.pipe(ofType(wordsActions.loadWordsSuccess));
    }

    public fetchWords(): Observable<Word[]> {
        return this.wordsApi.getWords();
    }

    constructor(
        private readonly actions$: Actions,
        private readonly wordsApi: WordsApi,
        private readonly wordsFacade: WordsFacade
    ) {}
}
