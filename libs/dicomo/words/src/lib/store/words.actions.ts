import { createAction, props } from '@ngrx/store';
import {
    LoadWordsFailurePayload,
    LoadWordsSuccessPayload,
    SetSelectWordPayload,
} from './words.payloads.interface';

const loadWords = createAction('[Words] Load Words');

const loadWordsSuccess = createAction(
    '[Words] Load Words Success',
    props<LoadWordsSuccessPayload>()
);

const loadWordsFailure = createAction(
    '[Words] Load Words Failure',
    props<LoadWordsFailurePayload>()
);

const setSelectedWord = createAction(
    '[Words] Set selected Word',
    props<SetSelectWordPayload>()
);

const selectRandomWord = createAction('[Words] Select random Word');

export const wordsActions = {
    loadWords,
    loadWordsSuccess,
    loadWordsFailure,
    setSelectedWord,
    selectRandomWord,
};
