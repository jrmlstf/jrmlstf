import { Word } from '@jrmlstf/dicomo/types';
import { EntityState } from '@ngrx/entity';
import { WORDS_FEATURE_KEY } from './words.feature-key';

export interface WordsState extends EntityState<Word> {
    error?: string | null;
    loaded: boolean;
    selectedId?: number;
}

export interface WordsFeatureState {
    readonly [WORDS_FEATURE_KEY]: WordsState;
}
