import { Word } from '@jrmlstf/dicomo/types';
import { Dictionary } from '@ngrx/entity';
import { createFeatureSelector, createSelector } from '@ngrx/store';
import { wordsAdapter } from './words.entity-adapter';
import { WORDS_FEATURE_KEY } from './words.feature-key';
import { WordsFeatureState, WordsState } from './words.state';

const getWordsState = createFeatureSelector<WordsFeatureState, WordsState>(
    WORDS_FEATURE_KEY
);

const { selectAll, selectEntities, selectTotal } = wordsAdapter.getSelectors();

const getWordsLoaded = createSelector(
    getWordsState,
    (state: WordsState) => state.loaded
);

const getWordsError = createSelector(
    getWordsState,
    (state: WordsState) => state.error
);

const getAllWords = createSelector(getWordsState, (state: WordsState) =>
    selectAll(state)
);

const getWordsEntities = createSelector(getWordsState, (state: WordsState) =>
    selectEntities(state)
);

const getSelectedId = createSelector(
    getWordsState,
    (state: WordsState) => state.selectedId
);

const getSelected = createSelector(
    getWordsEntities,
    getSelectedId,
    (entities: Dictionary<Word>, selectedId: number) =>
        selectedId && entities[selectedId]
);

const getWordsTotal = createSelector(
    getWordsState,
    (state: WordsState) => Object.keys(state.entities).length
);

export const wordsSelectors = {
    getAllWords,
    getWordsEntities,
    getWordsError,
    getWordsLoaded,
    getWordsState,
    getSelected,
    getSelectedId,
    getWordsTotal,
};
