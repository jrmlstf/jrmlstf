import { Injectable } from '@angular/core';
import { Word } from '@jrmlstf/dicomo/types';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { wordsActions } from './words.actions';
import { wordsSelectors } from './words.selectors';

@Injectable({
    providedIn: 'root',
})
export class WordsFacade {
    public loaded$: Observable<boolean> = this.store$.pipe(
        select(wordsSelectors.getWordsLoaded)
    );

    public allWords$: Observable<Word[]> = this.store$.pipe(
        select(wordsSelectors.getAllWords)
    );

    public selectedWord$: Observable<Word> = this.store$.pipe(
        select(wordsSelectors.getSelected)
    );

    public wordsTotal$: Observable<number> = this.store$.pipe(
        select(wordsSelectors.getWordsTotal)
    );

    constructor(private readonly store$: Store) {}

    public loadWords(): void {
        this.store$.dispatch(wordsActions.loadWords());
    }

    public selectRandomWord(): void {
        this.store$.dispatch(wordsActions.selectRandomWord());
    }
}
