import { TestBed } from '@angular/core/testing';
import { provideMockActions } from '@ngrx/effects/testing';
import { provideMockStore } from '@ngrx/store/testing';
import { DataPersistence, NxModule } from '@nrwl/angular';
import { hot } from '@nrwl/angular/testing';
import { Observable } from 'rxjs';
import * as WordsActions from './words.actions';
import { WordsEffects } from './words.effects';

describe('WordsEffects', () => {
    let actions: Observable<any>;
    let effects: WordsEffects;

    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [NxModule.forRoot()],
            providers: [
                WordsEffects,
                DataPersistence,
                provideMockActions(() => actions),
                provideMockStore(),
            ],
        });

        effects = TestBed.get(WordsEffects);
    });

    describe('loadWords$', () => {
        it('should work', () => {
            actions = hot('-a-|', { a: WordsActions.loadWords() });

            const expected = hot('-a-|', {
                a: WordsActions.loadWordsSuccess({ words: [] }),
            });

            expect(effects.loadWords$).toBeObservable(expected);
        });
    });
});
