import { Component, OnInit } from '@angular/core';
import { Word } from '@jrmlstf/dicomo/types';
import { Observable } from 'rxjs';
import { WordsFacade } from '../../store/words.facade';

@Component({
    selector: 'dicomo-words-list',
    templateUrl: './list.component.html',
    styleUrls: ['./list.component.scss'],
})
export class ListComponent implements OnInit {
    public words$: Observable<Word[]>;

    constructor(private readonly wordsFacade: WordsFacade) {}

    ngOnInit(): void {
        this.words$ = this.wordsFacade.allWords$;
    }
}
