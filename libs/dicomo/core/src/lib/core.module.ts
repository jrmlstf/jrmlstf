import { APP_INITIALIZER, NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouterModule } from '@angular/router';
import { environment } from '@jrmlstf/dicomo/environment';
import { RoutingModule } from '@jrmlstf/dicomo/routing';
import { WordsService, WordsStoreModule } from '@jrmlstf/dicomo/words';
import { EffectsModule } from '@ngrx/effects';
import {
    DefaultRouterStateSerializer,
    StoreRouterConnectingModule,
} from '@ngrx/router-store';
import { StoreModule } from '@ngrx/store';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { storeConfig } from './constants/config.constant';
import { loadDataFactory } from './factories/load-data.factory';

@NgModule({
    imports: [
        BrowserModule,
        BrowserAnimationsModule,
        RoutingModule,
        StoreModule.forRoot({}, storeConfig),
        EffectsModule.forRoot([]),
        !environment.production ? StoreDevtoolsModule.instrument() : [],
        StoreRouterConnectingModule.forRoot({
            serializer: DefaultRouterStateSerializer,
        }),
        WordsStoreModule,
    ],
    exports: [RouterModule],
    providers: [
        {
            provide: APP_INITIALIZER,
            multi: true,
            useFactory: loadDataFactory,
            deps: [WordsService],
        },
    ],
})
export class CoreModule {}
