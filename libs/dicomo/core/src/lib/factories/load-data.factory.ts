import { WordsService } from '@jrmlstf/dicomo/words';
import { take } from 'rxjs/operators';

export function loadDataFactory(
    wordsService: WordsService
): () => Promise<void> {
    return () =>
        new Promise((resolve: () => void, _: () => void): void => {
            wordsService
                .loadWordsAndGetNotifiedWhenSuccess()
                .pipe(take(1))
                .subscribe(() => resolve());
        });
}
