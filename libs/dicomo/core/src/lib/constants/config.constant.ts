import { environment } from '@jrmlstf/dicomo/environment';

export const storeConfig = {
    metaReducers: !environment.production ? [] : [],
    runtimeChecks: {
        strictStateImmutability: true,
        strictActionImmutability: true,
        strictStateSerializability: true,
        strictActionSerializability: true,
        strictActionWithinNgZone: true,
        strictActionTypeUniqueness: true,
    },
};
