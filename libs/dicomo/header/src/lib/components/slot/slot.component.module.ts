import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { AddWordComponentModule } from '../add-word/add-word.component.module';
import { CloseBtnComponentModule } from '../close-btn/close-btn.component.module';
import { LogoComponentModule } from '../logo/logo.component.module';
import { SlotComponent } from './slot.component';

@NgModule({
    declarations: [SlotComponent],
    imports: [
        CommonModule,
        RouterModule,
        LogoComponentModule,
        AddWordComponentModule,
        CloseBtnComponentModule,
    ],
    exports: [SlotComponent],
})
export class SlotComponentModule {}
