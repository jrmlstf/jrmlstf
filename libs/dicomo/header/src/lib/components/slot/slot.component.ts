import { Component, Input } from '@angular/core';
import { HeaderItemsEnum } from '../../enums/header-items.enum';
import { HeaderItem } from '../../interfaces/header-item.interface';

@Component({
    selector: 'dicomo-header-slot',
    templateUrl: './slot.component.html',
    styleUrls: ['./slot.component.scss'],
})
export class SlotComponent {
    @Input() public readonly items: HeaderItem[];

    public readonly headerComponentsEnum: typeof HeaderItemsEnum = HeaderItemsEnum;
}
