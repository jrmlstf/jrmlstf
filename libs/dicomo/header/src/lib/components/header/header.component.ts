import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { HeaderSlotEnum } from '../../enums/header-slots.enum';
import { HeaderItem } from '../../interfaces/header-item.interface';
import { HeaderService } from '../../services/header.service';

@Component({
    selector: 'dicomo-header',
    templateUrl: './header.component.html',
    styleUrls: ['./header.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class HeaderComponent implements OnInit {
    public leftSlotItem$: Observable<HeaderItem[]>;
    public middleSlotItem$: Observable<HeaderItem[]>;
    public rightSlotItem$: Observable<HeaderItem[]>;

    constructor(private readonly headerService: HeaderService) {}

    public ngOnInit(): void {
        this.leftSlotItem$ = this.headerService.getHeaderConfigForSlot(
            HeaderSlotEnum.LEFT
        );
        this.middleSlotItem$ = this.headerService.getHeaderConfigForSlot(
            HeaderSlotEnum.MIDDLE
        );
        this.rightSlotItem$ = this.headerService.getHeaderConfigForSlot(
            HeaderSlotEnum.RIGHT
        );
    }
}
