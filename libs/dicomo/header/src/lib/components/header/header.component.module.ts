import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { SlotComponentModule } from '../slot/slot.component.module';
import { HeaderComponent } from './header.component';

@NgModule({
    declarations: [HeaderComponent],
    imports: [CommonModule, RouterModule, SlotComponentModule],
    exports: [HeaderComponent],
})
export class HeaderComponentModule {}
