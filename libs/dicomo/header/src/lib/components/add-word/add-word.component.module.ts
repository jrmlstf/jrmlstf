import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { AddWordComponent } from './add-word.component';

@NgModule({
    declarations: [AddWordComponent],
    imports: [CommonModule, RouterModule],
    exports: [AddWordComponent],
})
export class AddWordComponentModule {}
