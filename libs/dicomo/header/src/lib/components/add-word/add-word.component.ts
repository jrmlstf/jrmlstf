import { Component } from '@angular/core';

@Component({
    selector: 'dicomo-header-add-word',
    templateUrl: './add-word.component.html',
    styleUrls: ['./add-word.component.scss'],
})
export class AddWordComponent {}
