import { Component } from '@angular/core';

@Component({
    selector: 'dicomo-header-close-btn',
    templateUrl: './close-btn.component.html',
    styleUrls: ['./close-btn.component.scss'],
})
export class CloseBtnComponent {}
