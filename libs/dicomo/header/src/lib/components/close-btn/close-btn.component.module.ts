import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { MatIconModule } from '@angular/material/icon';
import { RouterModule } from '@angular/router';
import { CloseBtnComponent } from './close-btn.component';

@NgModule({
    declarations: [CloseBtnComponent],
    imports: [CommonModule, RouterModule, MatIconModule],
    exports: [CloseBtnComponent],
})
export class CloseBtnComponentModule {}
