import { Action, createReducer, on } from '@ngrx/store';
import { wordsActions } from './words.actions';
import { wordsAdapter } from './words.entity-adapter';
import {
    LoadWordsFailurePayload,
    LoadWordsSuccessPayload,
    SetSelectWordPayload,
} from './words.payloads.interface';
import { WordsState } from './words.state';

export const initialState: WordsState = wordsAdapter.getInitialState({
    loaded: false,
});

const reducer = createReducer(
    initialState,
    on(
        wordsActions.loadWords,
        (state: WordsState): WordsState => ({
            ...state,
            loaded: false,
            error: null,
        })
    ),
    on(
        wordsActions.loadWordsSuccess,
        (state: WordsState, { words }: LoadWordsSuccessPayload): WordsState =>
            wordsAdapter.setAll(words, { ...state, loaded: true })
    ),
    on(
        wordsActions.loadWordsFailure,
        (
            state: WordsState,
            { error }: LoadWordsFailurePayload
        ): WordsState => ({
            ...state,
            error,
        })
    ),
    on(
        wordsActions.setSelectedWord,
        (state: WordsState, { id }: SetSelectWordPayload): WordsState => ({
            ...state,
            selectedId: id,
        })
    )
);

export function wordsReducer(
    state: WordsState | undefined,
    action: Action
): WordsState {
    return reducer(state, action);
}
