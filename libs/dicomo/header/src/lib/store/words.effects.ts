import { Injectable } from '@angular/core';
import { getRandomInt } from '@jrmlstf/common/utils';
import { Word } from '@jrmlstf/dicomo/types';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { Action } from '@ngrx/store';
import { fetch } from '@nrwl/angular';
import { Observable, of } from 'rxjs';
import { concatMap, map, withLatestFrom } from 'rxjs/operators';
import { WordsService } from '../services/words.service';
import { wordsActions } from './words.actions';
import { WordsFacade } from './words.facade';

@Injectable()
export class WordsEffects {
    public readonly loadWords$: Observable<Action> = createEffect(() =>
        this.actions$.pipe(
            ofType(wordsActions.loadWords),
            fetch({
                run: () => {
                    return this.wordsService.fetchWords().pipe(
                        map((words: Word[]) =>
                            wordsActions.loadWordsSuccess({
                                words: words,
                            })
                        )
                    );
                },

                onError: () => {
                    // we can log and error here and return null
                    // we can also navigate back
                    return null;
                },
            })
        )
    );

    public readonly selectRandomWord$: Observable<Action> = createEffect(() =>
        this.actions$.pipe(
            ofType(
                wordsActions.loadWordsSuccess,
                wordsActions.selectRandomWord
            ),
            concatMap((action: Action) =>
                of(action).pipe(withLatestFrom(this.wordsFacade.wordsTotal$))
            ),
            map(([_, wordsTotal]: [Action, number]) => {
                const randomlySelectedWordId = getRandomInt(1, wordsTotal);
                return wordsActions.setSelectedWord({
                    id: randomlySelectedWordId,
                });
            })
        )
    );

    constructor(
        private readonly actions$: Actions,
        private readonly wordsService: WordsService,
        private readonly wordsFacade: WordsFacade
    ) {}
}
