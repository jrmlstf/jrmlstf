import { WordsEntity } from './words.models';
import { initialState, wordsAdapter } from './words.reducer';
import * as WordsSelectors from './words.selectors';

describe('Words Selectors', () => {
    const ERROR_MSG = 'No Error Available';
    const getWordsId = (it) => it['id'];
    const createWordsEntity = (id: string, name = '') =>
        ({
            id,
            name: name || `name-${id}`,
        } as WordsEntity);

    let state;

    beforeEach(() => {
        state = {
            words: wordsAdapter.setAll(
                [
                    createWordsEntity('PRODUCT-AAA'),
                    createWordsEntity('PRODUCT-BBB'),
                    createWordsEntity('PRODUCT-CCC'),
                ],
                {
                    ...initialState,
                    selectedId: 'PRODUCT-BBB',
                    error: ERROR_MSG,
                    loaded: true,
                }
            ),
        };
    });

    describe('Words Selectors', () => {
        it('getAllWords() should return the list of Words', () => {
            const results = WordsSelectors.getAllWords(state);
            const selId = getWordsId(results[1]);

            expect(results.length).toBe(3);
            expect(selId).toBe('PRODUCT-BBB');
        });

        it('getSelected() should return the selected Entity', () => {
            const result = WordsSelectors.getSelected(state);
            const selId = getWordsId(result);

            expect(selId).toBe('PRODUCT-BBB');
        });

        it("getWordsLoaded() should return the current 'loaded' status", () => {
            const result = WordsSelectors.getWordsLoaded(state);

            expect(result).toBe(true);
        });

        it("getWordsError() should return the current 'error' state", () => {
            const result = WordsSelectors.getWordsError(state);

            expect(result).toBe(ERROR_MSG);
        });
    });
});
