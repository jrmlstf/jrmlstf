import { NgModule } from '@angular/core';
import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';
import { WordsEffects } from './words.effects';
import { WORDS_FEATURE_KEY } from './words.feature-key';
import { wordsReducer } from './words.reducer';

@NgModule({
    imports: [
        StoreModule.forFeature(WORDS_FEATURE_KEY, wordsReducer),
        EffectsModule.forFeature([WordsEffects]),
    ],
})
export class WordsStoreModule {}
