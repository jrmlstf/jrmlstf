import * as WordsActions from './words.actions';
import { WordsEntity } from './words.models';
import { initialState, reducer, State } from './words.reducer';

describe('Words Reducer', () => {
    const createWordsEntity = (id: string, name = '') =>
        ({
            id,
            name: name || `name-${id}`,
        } as WordsEntity);

    beforeEach(() => {});

    describe('valid Words actions', () => {
        it('loadWordsSuccess should return set the list of known Words', () => {
            const words = [
                createWordsEntity('PRODUCT-AAA'),
                createWordsEntity('PRODUCT-zzz'),
            ];
            const action = WordsActions.loadWordsSuccess({ words });

            const result: State = reducer(initialState, action);

            expect(result.loaded).toBe(true);
            expect(result.ids.length).toBe(2);
        });
    });

    describe('unknown action', () => {
        it('should return the previous state', () => {
            const action = {} as any;

            const result = reducer(initialState, action);

            expect(result).toBe(initialState);
        });
    });
});
