import { Word } from '@jrmlstf/dicomo/types';
import { createEntityAdapter, EntityAdapter } from '@ngrx/entity';

export const wordsAdapter: EntityAdapter<Word> = createEntityAdapter<Word>();
