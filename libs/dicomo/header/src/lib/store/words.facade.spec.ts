import { NgModule } from '@angular/core';
import { TestBed } from '@angular/core/testing';
import { EffectsModule } from '@ngrx/effects';
import { Store, StoreModule } from '@ngrx/store';
import { NxModule } from '@nrwl/angular';
import { readFirst } from '@nrwl/angular/testing';
import * as WordsActions from './words.actions';
import { WordsEffects } from './words.effects';
import { WordsFacade } from './words.facade';
import { WordsEntity } from './words.models';
import { reducer, State, WORDS_FEATURE_KEY } from './words.reducer';

interface TestSchema {
    words: State;
}

describe('WordsFacade', () => {
    let facade: WordsFacade;
    let store: Store<TestSchema>;
    const createWordsEntity = (id: string, name = '') =>
        ({
            id,
            name: name || `name-${id}`,
        } as WordsEntity);

    beforeEach(() => {});

    describe('used in NgModule', () => {
        beforeEach(() => {
            @NgModule({
                imports: [
                    StoreModule.forFeature(WORDS_FEATURE_KEY, reducer),
                    EffectsModule.forFeature([WordsEffects]),
                ],
                providers: [WordsFacade],
            })
            class CustomFeatureModule {}

            @NgModule({
                imports: [
                    NxModule.forRoot(),
                    StoreModule.forRoot({}),
                    EffectsModule.forRoot([]),
                    CustomFeatureModule,
                ],
            })
            class RootModule {}
            TestBed.configureTestingModule({ imports: [RootModule] });

            store = TestBed.get(Store);
            facade = TestBed.get(WordsFacade);
        });

        /**
         * The initially generated facade::loadAll() returns empty array
         */
        it('loadAll() should return empty list with loaded == true', async (done) => {
            try {
                let list = await readFirst(facade.allWords$);
                let isLoaded = await readFirst(facade.loaded$);

                expect(list.length).toBe(0);
                expect(isLoaded).toBe(false);

                facade.dispatch(WordsActions.loadWords());

                list = await readFirst(facade.allWords$);
                isLoaded = await readFirst(facade.loaded$);

                expect(list.length).toBe(0);
                expect(isLoaded).toBe(true);

                done();
            } catch (err) {
                done.fail(err);
            }
        });

        /**
         * Use `loadWordsSuccess` to manually update list
         */
        it('allWords$ should return the loaded list; and loaded flag == true', async (done) => {
            try {
                let list = await readFirst(facade.allWords$);
                let isLoaded = await readFirst(facade.loaded$);

                expect(list.length).toBe(0);
                expect(isLoaded).toBe(false);

                facade.dispatch(
                    WordsActions.loadWordsSuccess({
                        words: [
                            createWordsEntity('AAA'),
                            createWordsEntity('BBB'),
                        ],
                    })
                );

                list = await readFirst(facade.allWords$);
                isLoaded = await readFirst(facade.loaded$);

                expect(list.length).toBe(2);
                expect(isLoaded).toBe(true);

                done();
            } catch (err) {
                done.fail(err);
            }
        });
    });
});
