import { Word } from '@jrmlstf/dicomo/types';

export interface LoadWordsSuccessPayload {
    words: Word[];
}

export interface LoadWordsFailurePayload {
    error: any;
}

export interface SetSelectWordPayload {
    id: number;
}
