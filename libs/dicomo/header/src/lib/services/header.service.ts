import { Injectable } from '@angular/core';
import { ChildActivationEnd, Event, Router } from '@angular/router';
import { extractActivatedRouteSnapshotFirstChild } from '@jrmlstf/common/utils';
import { Observable } from 'rxjs';
import { distinctUntilChanged, filter, map } from 'rxjs/operators';
import { headerConfigKey } from '../constants/header-config-key.constant';
import { HeaderSlotEnum } from '../enums/header-slots.enum';
import { HeaderConfig } from '../interfaces/header-config.interface';
import { HeaderItem } from '../interfaces/header-item.interface';

@Injectable({
    providedIn: 'root',
})
export class HeaderService {
    constructor(private readonly router: Router) {}

    public getHeaderConfigForSlot(
        slot: HeaderSlotEnum
    ): Observable<HeaderItem[]> {
        return this.getHeaderConfig().pipe(
            map((headerConfig: HeaderConfig) =>
                headerConfig ? headerConfig[slot] : null
            )
        );
    }

    private getHeaderConfig(): Observable<HeaderConfig> {
        return this.router.events.pipe(
            filter((event: Event) => event instanceof ChildActivationEnd),
            distinctUntilChanged(),
            map(
                (event: ChildActivationEnd) =>
                    extractActivatedRouteSnapshotFirstChild(event.snapshot)
                        .data[headerConfigKey]
            )
        );
    }
}
