import { HeaderItemsEnum } from '../enums/header-items.enum';

export interface HeaderItem {
    type: HeaderItemsEnum;
    config?: any;
}
