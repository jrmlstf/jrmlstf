import { HeaderSlotEnum } from '../enums/header-slots.enum';
import { HeaderItem } from './header-item.interface';

export interface HeaderConfig {
    readonly [HeaderSlotEnum.LEFT]: HeaderItem[];
    readonly [HeaderSlotEnum.MIDDLE]: HeaderItem[];
    readonly [HeaderSlotEnum.RIGHT]: HeaderItem[];
}
