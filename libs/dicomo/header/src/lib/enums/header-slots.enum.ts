export enum HeaderSlotEnum {
    LEFT = 'left',
    MIDDLE = 'middle',
    RIGHT = 'right',
}
