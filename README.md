# Jrmlstf

This workspace is managed using [Nx](https://nx.dev).

## Applications

### Dicomo

**Dicomo** is a small app built with Angular to help you remember new words you discovered.

### Ciboulette

**Dicomo** is a serious game application built with Angular. It helps you learn new knowledge about botanic.

### DAC

**DAC** is the Next.js website of a french bed & breakfast located in the South West.
